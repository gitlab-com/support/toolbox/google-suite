const AmerApac = 0;
const ApacEmea = 1;
const EmeaAmer = 2;
const regionPairName = ["AMER<>APAC-E", "APAC<>EMEA", "EMEA<>AMER"];

/* Function: zeroPad()
   Parameters: number - any integer less than 100
   Purpose: to return the number as a 2-character string, padding with a leading 0 if necessary
*/
function zeroPad(number) {
  return (number < 10 ? "0" : "") + number;
}

/* Function: getNextFriday()
   Parameters: none
   Purpose:  To calculate the number of days until the next Friday, and return a string
             containing the date of that Friday. Note that if today is a Friday, the
             next Friday is 7 days from now. 
*/
function getNextFriday() {
  let today = new Date(), nextFriday = new Date();
  const dayOfWeek = today.getDay();
  const daysTillFriday = 7 - ((dayOfWeek + 2) % 7); // This weird math actually tells us how many days till the NEXT Friday
  const millisecondsTillFriday = daysTillFriday*24*60*60*1000;

  nextFriday.setTime(today.getTime() + millisecondsTillFriday);

  return( nextFriday.getFullYear() + "-" + zeroPad(nextFriday.getMonth() + 1) + "-" + zeroPad(nextFriday.getDate()) );
}

/* Function: createAgendaFromTemplate()
   Parameters: region = one of the constants: AmerApac, ApacEmea, EmeaAmer
   Purpose: To find the section of the current document that is the agenda template and
            to use that to start an agenda in the document for the next meeting
            of the indicated region (which is actually a region pair).
*/
function createAgendaFromTemplate(region) {
  var thisDoc = DocumentApp.getActiveDocument();
  var rangeBuilder = thisDoc.newRange();
  var docBody = thisDoc.getBody();
  var totalElements = docBody.getNumChildren();
  var element, newElement, type;
  var rangeIndex, rangeElements, rangeElement;
  var index = 0; // Indicates our current position within the document
  var listItemDictionary = {};
  var listItem, listID, newListItem;

  /* Look for the element in the document that has the text
     "<<START AGENDA TEMPLATE>>", which is what I've chosen
     to indicate where our template begins. All elements
     following this are part of the template until the
     end element is found. The end element contains only
     the text "<<END AGENDA TEMPLATE>>". */
  do {
    element = docBody.getChild(index++);
    if( element.getType() == DocumentApp.ElementType.PARAGRAPH && element.asParagraph().getText().search(/^<<START AGENDA TEMPLATE>>$/) != -1) {
      break;
    }
  } while (index < totalElements);

  /* If the loop continued to the end of the
     document, then no start element was found. */
  if (index == totalElements) {
    Logger.log('Did not find agenda beginning');
    return;
  }

  /* Loop through each successive element in the document, adding
     each one to a range using the rangeBuilder object until
     the end element is found. */
  do {
    element = docBody.getChild(index++);
    if( element.getType() == DocumentApp.ElementType.PARAGRAPH && element.asParagraph().getText().search(/^<<END AGENDA TEMPLATE>>$/) != -1) {
      /* In this document we've placed a horizontal line as a visual separator
         between the beginning elements of the document - introduction, instructions,
         links and the agenda template - and the body of the document - the actual
         agendas.
         To position the cursor at the location in the document at which we
         want to insert the new agenda, we'll move past the horizontal line
         by incrementing the index. */
      index++;
      break;
    } else {
      rangeBuilder.addElement(element);
    }
  } while (index < totalElements);

  /* Indicate an error if no end element was found */
  if (index == totalElements) {
    Logger.log('Did not find agenda ending');
    return;
  }

  /* Loop through all the elements in the range, inserting each one
     into the document using the new cursor position. */
  rangeElements = rangeBuilder.getRangeElements();
  totalElements = rangeElements.length;

  for ( rangeIndex = 0 ; rangeIndex < totalElements; rangeIndex++, ++index ) {
    rangeElement = rangeElements[rangeIndex].getElement();
    type = rangeElement.getType();
    if( type == DocumentApp.ElementType.PARAGRAPH ) {
      if ( rangeElement.asParagraph().getText().search(/<<FRIDAY>>/) != -1 ) {
        // Replace <<FRIDAY>> with Friday's date, and replace <<REGIONS>> with the region pair
        newElement = rangeElement.copy();
        newElement.asParagraph().replaceText("<<FRIDAY>>", getNextFriday());
        newElement.asParagraph().replaceText("<<REGIONS>>", regionPairName[region]);
        docBody.insertParagraph(index, newElement);
      } else {
        docBody.insertParagraph(index, rangeElement.copy());
      }
    }
    else if( type == DocumentApp.ElementType.TABLE) {
      docBody.insertTable(index, rangeElement.copy());
    }
    /* Lists are more complicated than the other element types. Essentially,
       in order to be sure that each list item is created as part of the
       correct list, with the correct glyph (bullet, number, etc.), we
       create one dummy temporary list item for each list, and refer to
       it as we create real list items for that list.
    */
    else if( type == DocumentApp.ElementType.LIST_ITEM) {
      listItem = rangeElement.copy().asListItem();
      listID = listItem.getListId();

      /* If there's no entry in our list "dictionary" for this
         element's List ID, that means we haven't yet seen this
         List ID, which means it's a new list. So:
         1. Add a dummy, temporary, list item at the end of the doc
         2. Save that item's element ID in our dictionary, indexed by its List ID
         3. Add a temporary paragraph element at the end of the doc
            and save its element ID in the the dictionary, indexed by the
            dummy list's ID with a "2" on the end to make it unique.
            Without the paragraph element here, when we create
            another dummy list item it will be associated with
            this list item and List ID. But we want each new
            dummy to be its own list.
      */
      if (listItemDictionary[listID] == null) {
        newListItem = docBody.appendListItem("temp");
        listItemDictionary[listID] = newListItem;
        listItemDictionary[listID + "2"] = docBody.appendParagraph("");
      }
      // Insert the new list item at the cursor
      docBody.insertListItem(index, listItem);
      // Give the new list item the same List ID as the previous one so
      // it'll be part of the same list.
      listItem.setListId(listItemDictionary[listID]);
      // Set the Glyph Type. Even though the list item has been
      // associated with an existing list, the setListId() function
      // likes to set the Glyph Type to the default, which is NUMBER
      listItem.setGlyphType(rangeElement.asListItem().getGlyphType());
    }
    else {
      // If there's a document element in your template that isn't
      // one of the types identified above, you'll need to add a new
      // clause to the if-then-else to handle it.
      Logger.log('Unhandled element type: ' + type);
    }
  }

  /* Loop through our dictionary to find and remove all dummy,
     temporary elements we created at the end of the doc.
  */
  if(listItemDictionary) {
    docBody.appendParagraph("");
    for(var key in listItemDictionary){
      listItemDictionary[key].clear().removeFromParent()
    }
  }
}

/* Function: createAmerApacAgenda()
   Parameters: none
   Purpose: To call the createAgendaFromTemplate() function with the
            correct regional pair indicated.
   Caller: "Create New Agenda" menu, "Create Amer<>APAC-E Agenda" menu item
*/
function createAmerApacAgenda() {
  createAgendaFromTemplate(AmerApac);
}

/* Function: createApacEmeaAgenda()
   Parameters: none
   Purpose: To call the createAgendaFromTemplate() function with the
            correct regional pair indicated.
   Caller: "Create New Agenda" menu, "Create Apac<>EMEA Agenda" menu item
*/
function createApacEmeaAgenda() {
  createAgendaFromTemplate(ApacEmea);
}

/* Function: createEmeaAmerAgenda()
   Parameters: none
   Purpose: To call the createAgendaFromTemplate() function with the
            correct regional pair indicated.
   Caller: "Create New Agenda" menu, "Create EMEA<>Amer Agenda" menu item
*/
function createEmeaAmerAgenda() {
  createAgendaFromTemplate(EmeaAmer);
}

/* Function: onOpen()
   Parameters: none
   Purpose: To create the "Create New Agenda" menu in the document's menu bar
   Caller: Automatically called when the document is opened
*/
function onOpen() {
  var ui = DocumentApp.getUi();
  var menu = ui.createMenu("Create New Agenda");

  menu.addItem("Create Amer<>APAC-E Agenda", "createAmerApacAgenda");
  menu.addItem("Create Apac<>EMEA Agenda", "createApacEmeaAgenda");
  menu.addItem("Create EMEA<>Amer Agenda", "createEmeaAmerAgenda");
  menu.addToUi();
}

/* Following are more functions for calculating other days. */

/*******************************************************************/
/* This finds the SOONEST Thursday, not the NEXT Thursday. So, if  */
/* today is Thursday, it returns today.                            */
/*******************************************************************/
function getSoonestThursday() {
  let today = new Date(), soonestThursday = new Date();
  const meetingDayOfWeek = 4; // Thursday
  const dayOfWeek = today.getDay();
  const daysTillThursday = ((meetingDayOfWeek - dayOfWeek + 7) % 7); // This math gives us the distance in days to the nearest Thursday
  const millisecondsTillThursday = daysTillThursday*24*60*60*1000;

  soonestThursday.setTime(today.getTime() + millisecondsTillThursday);

  return( soonestThursday.getFullYear() + "-" + zeroPad(soonestThursday.getMonth() + 1) + "-" + zeroPad(soonestThursday.getDate()) );
}

