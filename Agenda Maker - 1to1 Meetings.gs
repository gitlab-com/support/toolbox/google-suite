/* Function: zeroPad()
   Parameters: number - any integer less than 100
   Purpose: to return the number as a 2-character string, padding with a leading 0 if necessary
*/
function zeroPad(number) {
  return (number < 10 ? "0" : "") + number;
}

/* Function: createAgendaFromTemplate()
   Parameters: nextDateWish = passed to getDate(), specifies relative date for new agenda
   Purpose: To find the section of the current document that is the agenda template and
            to use that to start an agenda in the document for the next meeting
            of the indicated region.
*/
function createAgendaFromTemplate(nextDateWish) {
  var thisDoc = DocumentApp.getActiveDocument();
  var rangeBuilder = thisDoc.newRange();
  var docBody = thisDoc.getBody();
  var totalElements = docBody.getNumChildren();
  var element, newElement, type;
  var rangeIndex, rangeElements, rangeElement;
  var index = 0; // Indicates our current position within the document
  var listItemDictionary = {};
  var listItem, listID, newListItem;

  /* Look for the element in the document that has the text
     "<<START AGENDA TEMPLATE>>", which is what I've chosen
     to indicate where our template begins. All elements
     following this are part of the template until the
     end element is found. The end element contains only
     the text "<<END AGENDA TEMPLATE>>". */
  do {
    element = docBody.getChild(index++);
    if (element.getType() == DocumentApp.ElementType.PARAGRAPH && element.asParagraph().getText().search(/^<<START AGENDA TEMPLATE>>$/) != -1) {
      break;
    }
  } while (index < totalElements);

  /* If the loop continued to the end of the
     document, then no start element was found. */
  if (index == totalElements) {
    Logger.log('Did not find agenda beginning');
    return;
  }

  /* Loop through each successive element in the document, adding
     each one to a range using the rangeBuilder object until
     the end element is found. */
  do {
    element = docBody.getChild(index++);
    if (element.getType() == DocumentApp.ElementType.PARAGRAPH && element.asParagraph().getText().search(/^<<END AGENDA TEMPLATE>>$/) != -1) {
      /* In this document we've placed a horizontal line as a visual separator
         between the beginning elements of the document - introduction, instructions,
         links and the agenda template - and the body of the document - the actual
         agendas.
         To position the cursor at the location in the document at which we
         want to insert the new agenda, we'll move past the horizontal line
         by incrementing the index. */
      index++;
      break;
    } else {
      rangeBuilder.addElement(element);
    }
  } while (index < totalElements);

  /* Indicate an error if no end element was found */
  if (index == totalElements) {
    Logger.log('Did not find agenda ending');
    return;
  }

  /* Loop through all the elements in the range, inserting each one
     into the document using the new cursor position. */
  rangeElements = rangeBuilder.getRangeElements();
  totalElements = rangeElements.length;

  for (rangeIndex = 0; rangeIndex < totalElements; rangeIndex++, ++index) {
    rangeElement = rangeElements[rangeIndex].getElement();
    type = rangeElement.getType();
    if (type == DocumentApp.ElementType.PARAGRAPH) {
      if (rangeElement.asParagraph().getText().search(/<</) != -1) {
        // Replace placeholders
        newElement = rangeElement.copy();
        newElement.asParagraph().replaceText("<<DATE>>", getDate(nextDateWish));
        docBody.insertParagraph(index, newElement);
      }
      else {
        docBody.insertParagraph(index, rangeElement.copy());
      }
    }
    else if (type == DocumentApp.ElementType.TABLE) {
      docBody.insertTable(index, rangeElement.copy());
    }
    /* Lists are more complicated than the other element types. Essentially,
       in order to be sure that each list item is created as part of the
       correct list, with the correct glyph (bullet, number, etc.), we
       create one dummy temporary list item for each list, and refer to
       it as we create real list items for that list.
    */
    else if (type == DocumentApp.ElementType.LIST_ITEM) {
      listItem = rangeElement.copy().asListItem();
      listID = listItem.getListId();

      /* If there's no entry in our list "dictionary" for this
         element's List ID, that means we haven't yet seen this
         List ID, which means it's a new list. So:
         1. Add a dummy, temporary, list item at the end of the doc
         2. Save that item's element ID in our dictionary, indexed by its List ID
         3. Add a temporary paragraph element at the end of the doc
            and save its element ID in the the dictionary, indexed by the
            dummy list's ID with a "2" on the end to make it unique.
            Without the paragraph element here, when we create
            another dummy list item it will be associated with
            this list item and List ID. But we want each new
            dummy to be its own list.
      */
      if (listItemDictionary[listID] == null) {
        newListItem = docBody.appendListItem("temp");
        listItemDictionary[listID] = newListItem;
        listItemDictionary[listID + "2"] = docBody.appendParagraph("");
      }
      // Insert the new list item at the cursor
      docBody.insertListItem(index, listItem);

      // Give the new list item the same List ID as the previous one so
      // it'll be part of the same list.
      listItem.setListId(listItemDictionary[listID]);
      // Set the Glyph Type. Even though the list item has been
      // associated with an existing list, the setListId() function
      // likes to set the Glyph Type to the default, which is NUMBER
      listItem.setGlyphType(rangeElement.asListItem().getGlyphType());
    }
    else {
      // If there's a document element in your template that isn't
      // one of the types identified above, you'll need to add a new
      // clause to the if-then-else to handle it.
      Logger.log('Unhandled element type: ' + type);
    }
  }

  /* Loop through our dictionary to find and remove all dummy,
     temporary elements we created at the end of the doc.
  */
  if (listItemDictionary) {
    docBody.appendParagraph("");
    for (var key in listItemDictionary) {
      listItemDictionary[key].clear().removeFromParent()
    }
  }

  updatePreviousPriorities();
}


/* Menu helper functions */
function createAgendaFromTemplateToday() {
  createAgendaFromTemplate("today");
}
function createAgendaFromTemplateFromToday() {
  createAgendaFromTemplate("fromtoday");
}
function createAgendaFromTemplateFromLastMeeting() {
  createAgendaFromTemplate("fromlastmeeting");
}


/* Function: onOpen()
   Parameters: none
   Purpose: To create the "Create New Agenda" menu in the document's menu bar
   Caller: Automatically called when the document is opened
*/
function onOpen() {
  var ui = DocumentApp.getUi();
  var menu = ui.createMenu("Create New Agenda");

  menu.addItem("For today", "createAgendaFromTemplateToday");
  menu.addItem("For one week from today", "createAgendaFromTemplateFromToday");
  menu.addItem("For one week from last meeting", "createAgendaFromTemplateFromLastMeeting");
  menu.addToUi();
}

/* Function: getDate()
   Parameters: nextDateWish = specifies relative date for new agenda
   Purpose: Allows the user to specify which date should be replaced into the template
   Caller: createAgendaFromTemplate()
*/
function getDate(nextDateWish) {
  let nextDate = new Date();
  const millisecondsInAWeek = 7 * 24 * 60 * 60 * 1000;

  if (nextDateWish == "fromtoday") {
    nextDate.setTime((new Date()).getTime() + millisecondsInAWeek);
  } else if (nextDateWish == "fromlastmeeting") {
    var thisDoc = DocumentApp.getActiveDocument();
    var docBody = thisDoc.getBody();
    var totalElements = docBody.getNumChildren();
    var element, lastDateString = "";
    var index = 0; // Indicates our current position within the document

    do {
      element = docBody.getChild(index++);
      if (element.getType() == DocumentApp.ElementType.PARAGRAPH && element.asParagraph().getText().search(/^Agenda for <<DATE>>$/) != -1) {
        break;
      }
    } while (index < totalElements);

    if (index == totalElements) {
      Logger.log('Did not find date placeholder in agenda template');
      return;
    }

    do {
      element = docBody.getChild(index++);
      if (element.getType() == DocumentApp.ElementType.PARAGRAPH && element.asParagraph().getText().search(/^Agenda for /) != -1) {
        var lastDateString = element.asParagraph().getText().replace("Agenda for ", "");
        break;
      }
    } while (index < totalElements);



    if (lastDateString == "") {
      Logger.log('Did not find last agenda date in document');
      return;
    }

    let lastDate = new Date(lastDateString + "T09:10:00.000000000Z");
    const millisecondsInAWeek = 7 * 24 * 60 * 60 * 1000;

    nextDate.setTime(lastDate.getTime() + millisecondsInAWeek);
  }

  return (nextDate.getFullYear() + "-" + zeroPad(nextDate.getMonth() + 1) + "-" + zeroPad(nextDate.getDate()));
}

/* Function: getDate()
   Parameters: nextDateWish = specifies relative date for new agenda
   Purpose: Allows the user to specify which date should be replaced into the template
   Caller: createAgendaFromTemplate()
*/
function updatePreviousPriorities() {
  var thisDoc = DocumentApp.getActiveDocument();
  var rangeBuilder = thisDoc.newRange();
  var docBody = thisDoc.getBody();
  var totalElements = docBody.getNumChildren();
  var element;
  var followUpStart, followUpEnd, newFollowUpIndex;
  var index = 0; // Indicates our current position within the document
  var listItemDictionary2 = {};
  var listItem, listID, newListItem;


  do {
    element = docBody.getChild(index++);
    if (element.getType() == DocumentApp.ElementType.PARAGRAPH && element.asParagraph().getText().search(/^<<END AGENDA TEMPLATE>>$/) != -1) {
      break;
    }
  } while (index < totalElements);

  if (index == totalElements) {
    Logger.log('Did not find agenda template end');
    return;
  }


  Logger.log(index);
  Logger.log(element);
  Logger.log("after template end");

  do {
    element = docBody.getChild(index++);
    if (element.getType() == DocumentApp.ElementType.LIST_ITEM && element.asListItem().getText().search(/^Follow up from /) != -1) {
      newFollowUpIndex = index;
      break;
    }
  } while (index < totalElements);


  // we start again now that our index is _after_ the template, as we want to extract content from last week
  do {
    element = docBody.getChild(index++);
    if (element.getType() == DocumentApp.ElementType.LIST_ITEM && element.asListItem().getText().search(/^Ordered Priorities for/) != -1) {
      break;
    }
  } while (index < totalElements);

  // we do that twice, because we already inserted the new empty agenda. dirty hack.
  do {
    element = docBody.getChild(index++);
    if (element.getType() == DocumentApp.ElementType.LIST_ITEM && element.asListItem().getText().search(/^Ordered Priorities for/) != -1) {
      break;
    }
  } while (index < totalElements);

  if (index == totalElements) {
    Logger.log('Did not find older ordered priorities');
    return;
  }

  followUpStart = index;

  do {
    element = docBody.getChild(index++);
    Logger.log(index);
    Logger.log(element);

    if (element.getType() == DocumentApp.ElementType.PARAGRAPH && element.asParagraph().getText().search(/^Agenda for /) != -1) {
      // we arrived at the next meeting, i.e. we covered all ordered priorities of the week we're looking at
      Logger.log("break pa");
      break;
    } else if (element.getType() == DocumentApp.ElementType.LIST_ITEM && element.asListItem().getText().search(/^Agenda for /) != -1) {
      // we arrived at the next meeting, i.e. we covered all ordered priorities of the week we're looking at (odd behavior, sometimes it seems to be treated as a list?! this duplicate code should not be needed)
      Logger.log("break li");
      break;
    }
  } while (index < totalElements);

  followUpEnd = index - 2;

  rangeBuilder.addElementsBetween(docBody.getChild(followUpStart), docBody.getChild(followUpEnd));
  rangeBuilder.getSelectedElements().reverse().forEach(function (value) {
    Logger.log(value.getElement().asListItem().getText());
    listItem = value.getElement().copy().asListItem();
    listID = listItem.getListId();

    if (listItemDictionary2[listID] == null) {
      newListItem = docBody.appendListItem("temp");
      listItemDictionary2[listID] = newListItem;
      listItemDictionary2[listID + "2"] = docBody.appendParagraph("");
    }
    // Insert the new list item at the cursor
    docBody.insertListItem(newFollowUpIndex, listItem);

    // Give the new list item the same List ID as the previous one so
    // it'll be part of the same list.
    listItem.setListId(listItemDictionary2[listID]);
    // Set the Glyph Type. Even though the list item has been
    // associated with an existing list, the setListId() function
    // likes to set the Glyph Type to the default, which is NUMBER
    listItem.setGlyphType(value.getElement().asListItem().getGlyphType());
  });

  /* Loop through our dictionary to find and remove all dummy,
     temporary elements we created at the end of the doc.
  */
  if (listItemDictionary2) {
    docBody.appendParagraph("");
    for (var key in listItemDictionary2) {
      listItemDictionary2[key].clear().removeFromParent()
    }
  }
}
